<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '$6|6({0_t~E&|/L$r _SZvUjcb{vd)b@B}:b0!gJNE4Yud}++8ta+ScYTz^_E[Do' );
define( 'SECURE_AUTH_KEY',  '/rUD/$Wv=8I!fzK7O/o-d-;C+C_Tq-Z&acJj1-?VjTfaQ}^3w`]Ekhx|ubkm+|Ba' );
define( 'LOGGED_IN_KEY',    '0OFtD%N pVsnZdc;_0vT$|4v }Gn&i~QJm!I<]f7BKFsnVlYSx&/7R!Jct 67$EU' );
define( 'NONCE_KEY',        'Jc1:C0+RtGhP!6t]nMqYe-a@Vye1V&( 1O;DA}p#4E}1)U9&e!RZb|N[`1:v0xjO' );
define( 'AUTH_SALT',        '/Y5w_5p*+ 0<u6+c/7hXPm]@GkLiEkh/zDU|H`nr9/9`,[=L`+vVg|)h.7kRl5s*' );
define( 'SECURE_AUTH_SALT', '&:vAjtbK.x[WuOs=EP3]KpOVjw{5q_oM2CF$:9x?Ng:8rVO8zS.rIq.{>t)AkiBL' );
define( 'LOGGED_IN_SALT',   '4,?#^}!S?Y!eDyYbT;W$|}B-RpRZ42n6Og[;34M6r2LJ+N!G`S8qod$Fd-j(Y;hH' );
define( 'NONCE_SALT',       '+ax~K_<WWnqpngk?yrR9SVnG4.)B~Lb>U.!zL=ilsY8uFU)K@_WHsUr5A)mzc*o7' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
